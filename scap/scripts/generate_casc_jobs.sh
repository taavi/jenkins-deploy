#!/usr/bin/env bash

set -eu -o pipefail

(($# == 0)) && echo "Path to deployment dir needs to be specified" && exit 1
DEPLOYMENT_DIR=$1
CASC_DIR="$DEPLOYMENT_DIR"/conf/releasing/casc/

# shellcheck disable=SC2016
JOBS=$(sed -s 's/^/      /;$G' "$CASC_DIR"/jobs/*.groovy)
cat <<HERE >"$CASC_DIR"/jobs.yaml
jobs:
  - script: |
$JOBS
HERE
