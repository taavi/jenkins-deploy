
# Scap deployment

Calls to "scap deploy" should be done with the "-f" flag to make sure the deployment always takes place, even if there
are no file changes (we still want the Jenkins package to be updated).

There's no default deployment defined. In order to deploy, one of the two environments (ci & releasing) needs to be
specified. Bundled script "deploy.sh" can be used to perform a full deploy.

## Deployment specifics
### Deploying to https://releases-jenkins.wikimedia.org/

    ssh deployment.eqiad.wmnet
    cd /srv/deployment/releng/jenkins-deploy
    git pull --rebase
    ./deploy.sh

# Git LFS configuration

At the time of writing, Scap3 doesn't properly support LFS files. It tries to pull them from the deployment host, but
the "dumb" HTTP Git server there doesn't seem to be properly configured to serve the files.

A workaround for the problem is to specify an explicit URL in `.lfsconfig` where the LFS objects can be found. In our
case that means making the URL point to GitLab. Should the LFS storage location for this repository change in the
future, the URL in `.lsconfig` needs to be updated accordingly.

# Testing the deployment

We have a [development environment](https://gitlab.wikimedia.org/repos/releng/scap3-dev) that can be used to test
changes to the deployment. See the README there for more details.
